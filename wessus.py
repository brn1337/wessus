#!/usr/bin/env python3
# pylint: disable=anomalous-backslash-in-string

# Standard Libraries
import subprocess
import json
import sys
import urllib.request
import urllib.parse
from functools import lru_cache
import os
import json
import pprint
import argparse
import time
import csv
import pathlib
import re
import threading
import logging
from concurrent.futures import ThreadPoolExecutor

# Globals 
VERSION = "1.2"
WAPP_FLAGS_SLOW = [
	'-t 100 -D 1 -m 100 -w 100000 -r',
	'-t 100 -D 2 -m 200 -w 100000 -r',
	'-t 100 -D 4 -m 1000 -w 100000 -r'
]
VULNMON = "https://vulmon.com/searchpage?q={}&sortby=byrelevance"
CVEAPI = 'https://services.nvd.nist.gov/rest/json/cves/1.0?cpeMatchString={}'
logging.basicConfig(format='[%(asctime)s]:%(levelname)s: %(message)s', encoding='utf-8')
logger = logging.getLogger(__name__)


def banner():
	print("""
       Wessus v%s - Docker
  #1 for Cupressus Assessment""" % VERSION + color.GREEN + """
         ccee88oo
      C8O8O8Q8PoOb o8oo
     dOB69QO8PdUOpugoO9bD
    CgggbU8OU qOp qOdoUOdcb
        6OuU  """ + color.YELLOW + "/" + color.GREEN + "p u gcoUodpP" + color.YELLOW + """
          \\\//  /""" + color.GREEN + "douUP" + color.YELLOW + """
            \\\////
             |||/\\
             |||\/
             |||||
       .....//||||\....""" + color.BLUE + """    ===
     /"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\"\___/ ===
~~~ {~~ ~~~~ ~~~ ~~~~ ~~~ ~ /  ===- ~~~
     \______ o           __/
       \    \         __/
        \____\_______/
""" + color.END + """
AsciiArt by David Moore""")


class color:
   PURPLE = '\033[95m'
   CYAN = '\033[96m'
   DARKCYAN = '\033[36m'
   BLUE = '\033[94m'
   GREEN = '\033[92m'
   YELLOW = '\033[93m'
   RED = '\033[91m'
   BOLD = '\033[1m'
   UNDERLINE = '\033[4m'
   END = '\033[0m'


# This class wraps the URLs data into an object
class wessus:

	# array of software objects 
	tech = []

	def __init__(self, url):
		self.url = url
		self.tech = []

	def parse(self):
		# Load wappalizer and chromium
		wappalyzer, chromium = whereis('wappalyzer'), whereis('chromium')
		
		# Run wappalyzer
		cmd = f'CHROMIUM_BIN={chromium} {wappalyzer} "{self.url}" {wappalizer_param} 2>/dev/null'
		logger.debug(f'Starting wappalizer using this parameters: {cmd}')

		try:
			inv_raw = subprocess.check_output(cmd, encoding='UTF-8', shell=True)
		except subprocess.CalledProcessError as e:
			logger.error(f'Error in wappalyzer: {e}')
			exit(1)
	
		logger.debug(f'Wappalizer raw output: {inv_raw.strip()}')

		inv_pars = json.loads(inv_raw)
		for x in inv_pars["technologies"]:
			logger.debug(f'Parsing wappalizer output, current entry: {x}')
			self.tech.append(software(x['name'], x['version'], x['cpe'], x['categories'][0]))
		
		logger.info(f"Loaded {len(self.tech)} technologies for url: {self.url}")

	def get_data(self, debug=False):
		res = []
		for t in self.tech:
			logger.debug('Analyzing the following tech entry: %s' % t)			
			res.append(t.get_data())
		return {'url': self.url, 'tech': res}

	def parse_cve(self):
		with ThreadPoolExecutor(max_workers=10) as executor:
			for t in self.tech:
				executor.submit(software.get_cves, (t))

	def display(self, quiet=False):
		if args.quiet:
			print(u)
		else:
			print(color.BOLD + color.GREEN + f'\n{self.url}\n' + color.END)
		for r in self.tech:
			ver = r.version if r.version else "?"
			if args.quiet:
				print(
					r.name, ",", 
					r.version if r.version is not None else "", 
					",", r.cpe,
					",", ';'.join(r.cves)
					)
			else:
				print( color.BOLD + f'{r.name} v. {ver} ({r.cpe})' )
				if len(r.cves) != 0: 	
					print( f"\t{','.join(r.cves)}\n" + color.END )
				else:
					search = urllib.parse.quote(r.name + r.version)
					print( f"\tNone: {VULNMON.format(search)}\n" + color.END )


# This class wraps the software found by Wappalyzer into an object
class software:

	cpe = []
	
	def __init__(self, name, version=None, cpe=None, cat=None):
		self.name = name
		self.version = version if version else ""
		self.cpe = convert_cpe(cpe, version) if cpe else ""
		self.cat = cat
		self.cves = self.get_cves() if self.cpe != "" else ""

	def get_cves(self):
		return cpe2cve(self.cpe)	
	
	def get_data(self):
		return { 
			'name': self.name, 
			'version': self.version, 
			'cpe': self.cpe, 
			'category': self.cat, 
			'cves' : self.cves
			}


@lru_cache(maxsize=100)
def convert_cpe(wa_cpe, ver):
	cpe = str(wa_cpe).replace("/a","2.3:a").replace("/o","2.3:o")
	v = ver if ver else "*"
	padding = "*:*:*:*:*:*:*"
	return f"{cpe}:{v}:{padding}"


@lru_cache(maxsize=100)
def cpe2cve(cpe):
	query = CVEAPI.format(urllib.parse.quote(cpe))
	try:
		r = urllib.request.urlopen(query)
	except urllib.error.HTTPError:
		return []

	json_data = json.loads(r.read().decode('utf-8'))
	to_return = []
	for i in json_data['result']['CVE_Items']:
		to_return.append(i['cve']['CVE_data_meta']['ID'])
	
	return to_return


# https://github.com/AliasIO/wappalyzer/blob/master/src/drivers/webextension/_locales/en/messages.json
# https://www.wappalyzer.com/technologies
def load_cat(filename):
	with open(filename) as f:
		struct_cat = json.load(f)
	expl_cat, maj_cat, all_cat = [], [], []
	for key, value in struct_cat.items():
		all_cat.append(key)
		if 'E' in value[0]:
			expl_cat.append(key)
		if 'M' in value[0]:
			maj_cat.append(key)
	return all_cat, expl_cat, maj_cat


def whereis(app):
	command = 'where' if os.name == 'nt' else 'which'
	try:
		result = subprocess.check_output("{} {}".format(command, app), encoding='UTF-8', shell=True)
	except subprocess.CalledProcessError:
		logger.error(f'{app} not found')
		exit(1)
	return result.strip()


def read_file(f_path):
	parr = []
	if os.path.isfile(f_path):
		f = open(f_path, 'r')
		for line in f:
			parr.append(line.strip())
		f.close
	return parr


def cliparse():
	parser = argparse.ArgumentParser(
		description='', 
		epilog='Usage example: python3 %s -t http://www.example.com' % sys.argv[0]
		)
	url_group = parser.add_mutually_exclusive_group(required=True)
	url_group.add_argument('-u', 
		metavar='URL', 
		dest='url', 
		help='target url'
		)
	url_group.add_argument('-U', 
		metavar='URL_FILE', 
		dest='url_file', 
		help='target url file',
		type=pathlib.Path
		)
	parser.add_argument('-t',
		'--threads', 
		metavar='T', 
		dest='n_threads', 
		help='Number of threads',
		default=4,
		type=int
		)
	parser.add_argument('-c', 
		metavar='CAT_FILE', 
		dest='cat_file', 
		help='category file',
		default='./cat.json',
		type=pathlib.Path
		)
	parser.add_argument('-o', 
		metavar='OUTPUT_FILE', 
		dest='out_file', 
		help='file to save the results as json',
		type=pathlib.Path
		) 
	parser.add_argument("-s", '--slow', 
		help="takes more time to spyder the website", 
		action="count",
		default=0
		)
	verbosity = parser.add_mutually_exclusive_group()
	verbosity.add_argument("-v", '--verbose', 
		help="increase output verbosity (-vv for debug)", 
		action="count",
		default=0
		)
	verbosity.add_argument("-q", '--quiet', 
		help="suppress all messages except for the final one", 
		action="store_true"
		)
	return parser.parse_args()


def check_path(path, defpath="/usr/share/wessus"):
	if os.path.isabs(path):
		r = os.path.join(defpath, os.path.basename(path))
		logger.warning("A relative path is needed, switching to:" + r)
		return r
	return os.path.join(defpath, path)


def load_urls(url, url_file):
	if url:
		return [url]
	elif url_file:
		url_file = check_path(args.url_file, defpath=os.getenv('WORKDIR',os.getcwd()))
		return read_file(url_file)


if __name__ == '__main__':
	
	# Load user arguments, checks for mutex params are handled here
	args = cliparse()
	
	# Set loglevel
	# https://docs.python.org/3/howto/logging.html
	if args.verbose == 1:
		logger.setLevel(logging.INFO)
	elif args.verbose == 2:
		logger.setLevel(logging.DEBUG)
	else:
		logger.setLevel(logging.ERROR)

	# Store URL or URL list here in a list
	urls = load_urls(args.url, args.url_file)
	
	logger.info("Loaded " + str(len(urls)) + " URLs")
	logger.debug("URLS: " + str(urls))

	# Slowness of the configuration is handled here
	wappalizer_param = WAPP_FLAGS_SLOW[args.slow]
	
	# Load wappalizer categories to analyze
	if args.cat_file:
		cat_file = check_path(args.cat_file)
		all_cat, expl_cat, maj_cat = load_cat(cat_file)	

	if not args.quiet:
		# Show banner
		banner()
		# Start timer
		start_time = time.time()	

	parsing = []
	for u in urls:
		parsing.append(wessus(u))
		
	# Multithread parsing
	with ThreadPoolExecutor(max_workers=args.n_threads) as executor:
		for t in parsing:
			executor.submit(wessus.parse, (t))	

	raw_all = {'config': sys.argv, 'result': []}		
	for p in parsing:
		p.display(args.quiet)

		if args.out_file:
			raw = p.get_data()
			raw_all['result'].append(raw)

		if not args.quiet:
			print("Assessment completed in %.2f seconds\n" % (time.time() - start_time))

	if args.out_file:
		out_file = check_path(args.out_file, defpath=os.getenv('WORKDIR',os.getcwd()))
		open(out_file, 'w').write(json.dumps(raw_all))
