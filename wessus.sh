#!/bin/sh
# This is a wrapper for the docker version of wessus

docker run -it --rm -v /etc/passwd:/etc/passwd:ro -v "$PWD":/workdir --user "$(id -u)":"$(id -g)" wessus $@