FROM python:alpine

# Prerequisites
RUN apk add --update nodejs npm make chromium git
RUN ln -s /usr/bin/chromium-browser /usr/bin/chromium

# Configure
ENV PUPPETEER_SKIP_DOWNLOAD=true
ENV WORKDIR='/workdir'

# Copy files and set entrypoint
COPY . .

RUN make install

ENTRYPOINT ["/usr/bin/wessus"]