PKGNAME := wessus
PREFIX  ?= /usr

update:
	git pull --ff-only

install:
	command -v npm chromium >/dev/null
	npm i -g wappalyzer@6.8.1
	install -Dm755 ${PKGNAME}.py $(PREFIX)/bin/${PKGNAME}
	install -dm755 $(PREFIX)/share/${PKGNAME}
	install -Dm644 cat.json $(PREFIX)/share/${PKGNAME}/cat.json

docker: update
	docker build -t ${PKGNAME} .
	sudo install -Dm755 ${PKGNAME}.sh $(PREFIX)/bin/${PKGNAME}
