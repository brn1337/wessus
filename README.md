# Wessus - Docker version ![](img/logo-small.png)

Wessus identifies vulnerabilities in websites identifying technologies and checking related CVE for the specific version.
Big shout-out to both Wappalyzer and Vulmon for making this project possible!

# Usage

Simple scan

```bash
wessus -u http://example.com
```

Scan multiple urls and save output to file

```bash
wessus -u urls.txt -o out.csv
```

Detailed and slow scan

```bash
wessus -u https://example.com/profile -v -s
```

Very detailed and slow scan

```bash
wessus -u http://example.com -vv -ss
```

# Demo

![](img/demo.gif)

# Installation

```bash
git clone https://gitlab.com/brn1337/wessus.git
cd wessus

# Docker install
make docker

# Normal install
sudo make install

# On Archlinux (with AUR helper: eg 'paru')
paru -S wessus
```

# License

Wessus is licensed under [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1)

```
CC-BY-NC-SA This license requires that reusers give credit to the creator.
It allows reusers to distribute, remix, adapt, and build upon the material in any medium or format, for noncommercial purposes only.
If others modify or adapt the material, they must license the modified material under identical terms.
- Credit must be given to you, the creator.
- Only noncommercial use of your work is permitted.
- Adaptations must be shared under the same terms.
```
